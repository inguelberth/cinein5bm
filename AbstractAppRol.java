package org.inguelberth.app;

import java.util.HashMap;

import org.inguelberth.utilidades.Input;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.manejadores.ManejadorPelicula;
import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;

public abstract class AbstractAppRol{
	private Decodificador decode;
	private boolean connected;

	public AbstractAppRol(){
		this.connected=true;
	}
	
	public void setDecodificador(Decodificador decodificador){
		this.decode=decodificador;
	}
	public boolean isConnected(){
		return connected;
	}
	public void setConnected(boolean connected){
		this.connected=connected;
	}
	
	public void buscar(){

	}
	public void listar(){

	}
	public void imprimirAnuncios(Anuncio anuncio, int nivel){
		String espacios = obtenerLineaJerarquia(nivel);
		for(Anuncio anuncioH : anuncio.getListaAnuncio()){
			System.out.println(espacios+"|_____ "+anuncioH.getNombre());
			System.out.println(espacios+"       "+anuncioH.getSegundos());
			System.out.println(espacios+"       "+anuncioH.getPatrocinador());
			imprimirAnuncios(anuncioH, nivel+1);
		}
	}
	public String obtenerLineaJerarquia(int nivel){
		String lineas="";
		for(int indice=0;indice<nivel;indice++){
			lineas+="       ";
		}
		return lineas;
	}
	public void revisarAccionar(String accion, HashMap<String, String> objeto){
		switch(accion){
			case "list movies":
				for(Pelicula pelicula : ManejadorPelicula.getInstancia().obtenerLista()){
					System.out.println("----------------------------");
					System.out.println("Nombre: "+pelicula.getNombre());
					System.out.println("Genero: "+pelicula.getGenero().toString());
					System.out.println("Calificacion: "+pelicula.getCalificacion());

				}
				System.out.println("----------------------------");
				System.out.println("FIN DE LA LISTA :-)");
			break;
			case "show movie":
				for(Pelicula pelicula : ManejadorPelicula.getInstancia().obtenerLista()){
					System.out.println("----------------------------");
					System.out.println("Nombre: "+pelicula.getNombre());
					System.out.println("Genero: "+pelicula.getGenero().toString());
					System.out.println("Calificacion: "+pelicula.getCalificacion());
					System.out.println("-------ANUNCIOS---------");
					for(Anuncio anuncio : pelicula.getListaAnuncio()){
						System.out.println("|_____ "+anuncio.getNombre());
						System.out.println("       "+anuncio.getSegundos());
						System.out.println("       "+anuncio.getPatrocinador());
						imprimirAnuncios(anuncio, 2);
					}

				}
				System.out.println("----------------------------");
				System.out.println("FIN DE LA LISTA :-)");
			break;
			default:
				System.out.println("Comando no existe. :-(");
		}
	}
	public void iniciar(){
		String comando;
		do{
			System.out.print(ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getNombre()+" >>");
			comando=Input.getInstancia().leer();
			this.decode.decodificarComando(comando);
		}while(this.isConnected());
	}
}
