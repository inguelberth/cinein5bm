package org.inguelberth.utilidades;

import java.util.Scanner;

public class Input{
	private static Input instancia; //Singleton: Instancia
	
	private Scanner scaner;

	private Input(){ //Singleton: Constructor privado para que no se pued instanciar fuera de la clase
		scaner=new Scanner(System.in);
	}
	public String leer(){
		return scaner.nextLine();
	}
	public void cerrar(){
		scaner.close();
	}

	public static Input getInstancia(){ //Singleton: Metodo para obtener la instancia del Singleton
		if(instancia==null){
			instancia=new Input();
		}
		return instancia;
		
	}
}
