package org.inguelberth.manejadores;

import java.util.ArrayList;

import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;

public class ManejadorPelicula{
	private static ManejadorPelicula instancia;

	private ArrayList<Pelicula> listaPelicula;

	public ManejadorPelicula(){
		this.listaPelicula=new ArrayList<Pelicula>();
	}
	public ArrayList<Pelicula> obtenerLista(){
		return this.listaPelicula;
	}
	public void agregarPelicula(Pelicula pelicula){
		this.listaPelicula.add(pelicula);
	}
	public void eliminarPelicula(Pelicula pelicula){
		this.listaPelicula.remove(pelicula);
	}
	public Pelicula buscarPelicula(String nombre){
		for(Pelicula pelicula : this.obtenerLista()){
			if(pelicula.getNombre().equals(nombre))
				return pelicula;
		}
		return null;
	}
	public Anuncio buscarDentroDePeli(Pelicula pelicula, String nombre){
		for(Anuncio anuncio : pelicula.getListaAnuncio()){
			if(anuncio.getNombre().equals(nombre))
				return anuncio;
		}
		return null;
	}
	public Anuncio buscarDentroDeCorto(Anuncio anuncio, String nombre){
		for(Anuncio anuncioH : anuncio.getListaAnuncio()){
			if(anuncioH.getNombre().equals(nombre)){
				return anuncioH;
			}
		}
		return null;
	}
	public static ManejadorPelicula getInstancia(){
		if(instancia==null)
			instancia=new ManejadorPelicula();
		return instancia;
	}
	

}
