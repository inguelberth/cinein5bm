package org.inguelberth.beans;

import java.util.ArrayList;

public class Anuncio{
	private String nombre;
	private String patrocinador;
	private int segundos;
	private ArrayList<Anuncio> listaAnuncios;
	
	public void setListaAnuncio(ArrayList<Anuncio> lista){
		this.listaAnuncios=lista;
	}
	public 	ArrayList<Anuncio> getListaAnuncio(){
		return this.listaAnuncios;
	}

	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setSegundos(int segundos){
		this.segundos=segundos;
	}
	public int getSegundos(){
		return this.segundos;	
	}
	public void setPatrocinador(String patrocinador){
		this.patrocinador=patrocinador;
	}
	public String getPatrocinador(){
		return this.patrocinador;
	}
	public Anuncio(){
		setListaAnuncio(new ArrayList<Anuncio>());
	}
}
