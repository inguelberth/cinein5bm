package org.inguelberth.utilidades;

import java.util.HashMap;
import org.inguelberth.eventos.Comandos;

public class Decodificador{
	private Comandos escuchador;
	public Decodificador(){
		this.escuchador=null;
	}
	public void decodificarComando(String comando){
		String[] comandos = comando.split(" ");
		String accion = "";
		HashMap<String, String> objeto = new HashMap<String, String>();
		
		accion=comandos[0]+" "+comandos[1];

		for(int posicion=2;posicion<comandos.length;posicion++){
			String claveValor[] = comandos[posicion].split("=");
			objeto.put(claveValor[0], claveValor[1]);
		}
		if(escuchador!=null){
			escuchador.avisarAccionar(accion, objeto);
		}
	}
	public void addActionListener(Comandos interfaz){
		this.escuchador=interfaz;
	}
	
}
