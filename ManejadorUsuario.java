package org.inguelberth.manejadores;

import java.util.ArrayList;

import org.inguelberth.beans.Usuario;

public class ManejadorUsuario{
	private static ManejadorUsuario instancia;

	private ArrayList<Usuario> listaUsuarios;
	private Usuario usuarioAutenticado;

	private ManejadorUsuario(){
		this.listaUsuarios=new ArrayList<Usuario>();
		
		//Tres formas distintas de agregar objectos a un ArrayList

		Usuario u1=new Usuario("Inguelberth","estgarcia",21,"admin","123");
		this.listaUsuarios.add(u1); //Usuario posicion 0

		this.listaUsuarios.add(new Usuario("Jorge","jorgeperez",28,"admin","321")); //Usuario posicion 1

		Usuario u3 = new Usuario();
		u3.setNombre("Edgar");
		u3.setNick("edgar123");
		u3.setEdad(50);
		u3.setRol("empleado");
		u3.setPassword("guate123");
		this.listaUsuarios.add(u3); //Usuario posicion 2
	}
	public void agregarUsuario(Usuario usuario){
		this.listaUsuarios.add(usuario);
	}
	public void eliminarUsuario(Usuario usuario){
		this.listaUsuarios.remove(usuario);
	}
	public ArrayList<Usuario> obtenerListaUsuarios(){
		return this.listaUsuarios;
	}
	public Usuario buscarUsuario(String nick){
		/*FOREACH*/
		/*for(Usuario usuario : listaUsuarios){
			if (usuario.getNick().equals(nick)){
				return usuario;
			}
		}*/
		/*FOR*/
		for(int posicion=0;posicion<listaUsuarios.size();posicion++){
			if(this.listaUsuarios.get(posicion).getNick().equals(nick)){
				return this.listaUsuarios.get(posicion);
			}
		}
		return null;
	}
	public boolean autenticarUsuario(String nick, String password){
		Usuario usuario = this.buscarUsuario(nick);
		if(usuario!=null){
			if(usuario.getPassword().equals(password)){
				this.usuarioAutenticado=usuario;
				return true;
			}
		}
		return false;
	}
	public void desautenticarUsuario(){
		this.usuarioAutenticado=null;
	}
	public Usuario obtenerUsuarioAutenticado(){
		return this.usuarioAutenticado;
	}
	public static ManejadorUsuario getInstancia(){
		if(instancia==null){
			instancia=new ManejadorUsuario();
		}
		return instancia;
	}
}
