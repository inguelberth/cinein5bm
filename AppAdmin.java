package org.inguelberth.app;

import org.inguelberth.eventos.Comandos;
import org.inguelberth.beans.Usuario;
import org.inguelberth.beans.Pelicula;
import org.inguelberth.beans.Anuncio;
import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.manejadores.ManejadorPelicula;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.enumeration.Genero;

import java.util.HashMap;

public class AppAdmin extends AbstractAppRol implements Comandos{
	public AppAdmin(Decodificador decodificador){
		decodificador.addActionListener(this);
		super.setDecodificador(decodificador);
	}
	public void agregarUsuario(Usuario usuario){
		ManejadorUsuario.getInstancia().agregarUsuario(usuario);
		System.out.println("	Usuario agregado satisfactoriamente :)");
	}
	public void eliminarUsuario(String nick){
		Usuario usuarioEliminar = ManejadorUsuario.getInstancia().buscarUsuario(nick);
		if(usuarioEliminar!=null)
			ManejadorUsuario.getInstancia().eliminarUsuario(usuarioEliminar);
		System.out.println("	Usuario eliminado satisfactoriamente :)");
	}
	public void listarUsuarios(){
		for(Usuario usuario : ManejadorUsuario.getInstancia().obtenerListaUsuarios()){
			System.out.println("------------------------------------------------");
			System.out.println("Nombre: "+usuario.getNombre());
			System.out.println("Nick: "+usuario.getNick());
			System.out.println("Edad: "+usuario.getEdad());
			System.out.println("Rol: "+usuario.getRol());
			System.out.println("------------------------------------------------");
		}
		System.out.println("FIN DE LISTA ^_^");
	}
	public void avisarAccionar(String accion, HashMap<String, String> objeto){
		switch(accion){
			case "list user":
				this.listarUsuarios();
				break;
			case "add user":
				Usuario usuarioAgregar = new Usuario();
				usuarioAgregar.setNombre(objeto.get("nombre"));
				usuarioAgregar.setNick(objeto.get("nick"));
				usuarioAgregar.setEdad(Integer.parseInt(objeto.get("edad")));
				usuarioAgregar.setRol(objeto.get("rol"));
				usuarioAgregar.setPassword(objeto.get("password"));
				this.agregarUsuario(usuarioAgregar);
				break;
			case "remove user":
				this.eliminarUsuario(objeto.get("nick"));
				break;
			case "exit user":
				super.setConnected(false);
				break;
			case "add movie":
				Pelicula peliculaAgregar = new Pelicula();
				peliculaAgregar.setNombre(objeto.get("nombre"));
				peliculaAgregar.setCalificacion(Integer.parseInt(objeto.get("calificacion")));
				switch(objeto.get("genero")){
					case "terror":
						peliculaAgregar.setGenero(Genero.Terror);
					break;
					case "thriller":
						peliculaAgregar.setGenero(Genero.Thriller);
					break;
					case "comedia":
						peliculaAgregar.setGenero(Genero.Comedia);
					break;
					default:
						System.out.println("Genero no existente");
				}
				ManejadorPelicula.getInstancia().agregarPelicula(peliculaAgregar);
			break;
			case "add advert":
				Anuncio anuncioAgregar=new Anuncio();
				anuncioAgregar.setNombre(objeto.get("nombre"));
				anuncioAgregar.setSegundos(Integer.parseInt(objeto.get("segundos")));
				anuncioAgregar.setPatrocinador(objeto.get("patrocinador"));
				
				String down=objeto.get("down");

				String arbol[] = down.split(",");
				
				Pelicula pelicula = ManejadorPelicula.getInstancia().buscarPelicula(arbol[0]);

				if(arbol.length==1)
					pelicula.getListaAnuncio().add(anuncioAgregar);
				else{
					Anuncio anuncioR =ManejadorPelicula.getInstancia().buscarDentroDePeli(pelicula, arbol[1]);
					
					for(int posicion=2;posicion<arbol.length;posicion++){
						anuncioR = ManejadorPelicula.getInstancia().buscarDentroDeCorto(anuncioR, arbol[posicion]);
						System.out.print("  ->"+anuncioR.getNombre()+" ");
					}
					System.out.println("ANUNCIO AGREGADO A "+arbol[arbol.length-1]+" tamanio "+arbol.length);
					anuncioR.getListaAnuncio().add(anuncioAgregar);
				}
				System.out.println("Se a agregado un anuncio satisfactoriamente. :-)");	
				
			break;
			default:
				super.revisarAccionar(accion, objeto);
		}
	}
}
