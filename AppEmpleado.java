package org.inguelberth.app;

import java.util.HashMap;

import org.inguelberth.eventos.Comandos;
import org.inguelberth.utilidades.Decodificador;

public class AppEmpleado extends AbstractAppRol implements Comandos{

	public AppEmpleado(Decodificador decodificador){
		decodificador.addActionListener(this);
		super.setDecodificador(decodificador);
	}

	public void avisarAccionar(String accion, HashMap<String, String> objeto){
		super.revisarAccionar(accion, objeto);
	}
}
