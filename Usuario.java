package org.inguelberth.beans;
public class Usuario{
	private int edad;
	private String nombre;
	private String password;
	private String nick;
	private String rol;
	public void setEdad(int edad){
		this.edad=edad;
	}
	public int getEdad(){
		return this.edad;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setPassword(String password){
		this.password=password;
	}
	
	public String getPassword(){
		return this.password;
	}
	public void setNick(String nick){
		this.nick=nick;
	}
	public String getNick(){
		return this.nick;
	}
	public void setRol(String rol){
		this.rol=rol;
	}
	public String getRol(){
		return this.rol;
	}
	public Usuario(){
		super();
	}
	public Usuario(String nombre, String nick, int edad, String rol, String password){
		setNombre(nombre);
		setNick(nick);
		setEdad(edad);
		setRol(rol);
		setPassword(password);
	}	
}
