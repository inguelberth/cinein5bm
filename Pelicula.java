package org.inguelberth.beans;

import java.util.ArrayList;

import org.inguelberth.enumeration.Genero;

public class Pelicula{
	private String nombre;
	private Genero genero;
	private int calificacion;
	private ArrayList<Anuncio> listaAnuncios;

	public void setListaAnuncio(ArrayList<Anuncio> lista){
		this.listaAnuncios=lista;
	}
	public 	ArrayList<Anuncio> getListaAnuncio(){
		return this.listaAnuncios;
	}
	
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setGenero(Genero genero){
		this.genero=genero;
	}
	public Genero getGenero(){
		return this.genero;
	}
	public void setCalificacion(int calificacion){
		this.calificacion=calificacion;
	}
	public int getCalificacion(){
		return this.calificacion;
	}
	public Pelicula(){
		setListaAnuncio(new ArrayList<Anuncio>());
	}
}
