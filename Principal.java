package org.inguelberth.sistema;

import java.util.HashMap;

import org.inguelberth.manejadores.ManejadorUsuario;
import org.inguelberth.utilidades.Input;
import org.inguelberth.utilidades.Decodificador;
import org.inguelberth.app.AbstractAppRol;
import org.inguelberth.app.AppAdmin;
import org.inguelberth.app.AppEmpleado;

public class Principal{
	public void iniciar(){
		do{
			Decodificador decodificador = new Decodificador();
			AbstractAppRol appRol = null;

			String nick, password;
			System.out.println("Ingrese Nick:");
			nick = Input.getInstancia().leer();

			System.out.println("Ingrese contraseña:");
			password= Input.getInstancia().leer();

			boolean resultado = ManejadorUsuario.getInstancia().autenticarUsuario(nick, password);

			if(resultado==true){
				System.out.println("Bienvenido "+ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getNombre() + "!");
				switch(ManejadorUsuario.getInstancia().obtenerUsuarioAutenticado().getRol()){
					case "admin":
						appRol=new AppAdmin(decodificador);
						break;
					case "empleado":
						appRol=new AppEmpleado(decodificador);
						break;
					case "cliente":
						//appRol=new AppCliente();
						break;
					default:
						System.out.println("Lo sentimos, su Rol esta en construccion...");
				}
				appRol.iniciar();
				System.out.println("");
				System.out.println("	Bye :)");
				System.out.println("");
				System.out.println("");
			}else
				System.out.println("Sus credenciales son invalidas!");
		}while(true);
		
		//Input.getInstancia().cerrar();	
	}
	public void agregarUsuario(String resultado, HashMap<String, String> objeto){
		System.out.println("Se esta agregando el objeto con las siguientes propiedades");
		System.out.println("Nombre: "+objeto.get("nombre"));
		System.out.println("Edad: "+objeto.get("edad"));
		System.out.println("Rol: "+objeto.get("rol"));
		System.out.println("Nick: "+objeto.get("nick"));
	}
}
